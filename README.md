#### Changes on Webjive:

On file: *webjive/src/dashboard/components/RunCanvas/emmiter.ts* and *webjive/src/jive/state/api/tango/index.js*
 
Change:
```
function createSocket(tangoDB: string) {
  return new WebSocket(socketUrl(tangoDB), "graphql-ws");
}
```
To:
```
function createSocket(tangoDB: string) {
  var wsUri = "ws://127.0.0.1:1234";
  return new WebSocket(wsUri);
}
```

## Install on Linux:

```
sudo apt-get -y install g++ qt5-default libqt5widgets5* libqt5websockets5* libqt5webkit5*
cd webjivedeamon
qmake
make
./WebjiveDeamon
```

# Socket sender:

```
{"type":"start","payload":{"query":"\nsubscription Attributes($fullNames: [String]!) {\n  attributes(fullNames: $fullNames) {\n    device\n    attribute\n    value\n    writeValue\n    timestamp\n  }\n}","variables":{"fullNames":["sys/tg_test/1/double_scalar"]}}}
```

# Socket reciver:

```
{"type": "data", "payload": {"data": {"attributes": {"device": "sys/tg_test/1", "attribute": "double_scalar", "value": 181.01969624669448, "writeValue": 0.0, "timestamp": 1568211918.50133}}}}
```

## Inject to:

```
webjive/src/dashboard/components/RunCanvas/emmiter.ts
```

## or

```
webjive/src/jive/state/api/tango/index.js
```


