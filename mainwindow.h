#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtWebSockets>
#include <QtCore>
#include <QDebug>
#include <cstdio>
#include <QMainWindow>
#include <QTime>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QSet>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)
QT_FORWARD_DECLARE_CLASS(QString)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void WriteTextBox(QString txt);

private slots:

    void on_bt_send_clicked();

    void on_bt_start_clicked();

    void on_bt_clear_clicked();

    //WEB SOCKET

    void onNewConnection();

    void processMessage(const QString &message);

    void socketDisconnected();

    void sendSocket(QString data);

    void timerCheck();

    void on_bt_stop_clicked();

    QStringList devicesToList(QString str);

    QString getName(QString str);

    QString getType(QString str);

    void on_bt_one_clicked();

    void sendData();

private:
    Ui::MainWindow *ui;
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
    QTimer *timerSendSocket;
};

#endif // MAINWINDOW_H
