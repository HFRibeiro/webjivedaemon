#include "mainwindow.h"
#include "ui_mainwindow.h"

int port = 1234;

int numOfDevices = 0;
QStringList devicesList = {};

using namespace std;

QT_USE_NAMESPACE

//Recive example 1 Wid:

//{"type":"start","payload":{"query":"\nsubscription Attributes($fullNames: [String]!)
//{\n  attributes(fullNames: $fullNames)
//{\n    device\n    attribute\n    value\n    writeValue\n    timestamp\n  }\n}",
//"variables":{"fullNames":["sys/tg_test/1/double_scalar"]}}}

//Send example 1 Wid:

//{"type": "data", "payload": {"data": {"attributes":
//{"device": "sys/tg_test/1", "attribute": "double_scalar", "value": 57.58742210755554, "writeValue": 0.0, "timestamp": 1568282931.181805}}}}

//Recive example 2 Wid:

//{"type":"start","payload":{"query":"\nsubscription Attributes($fullNames: [String]!)
//{\n  attributes(fullNames: $fullNames)
//{\n    device\n    attribute\n    value\n    writeValue\n    timestamp\n  }\n}",
//"variables":{"fullNames":["sys/tg_test/1/double_scalar","sys/tg_test/1/short_scalar"]}}}

//Send example 2 Wid:

//{"type": "data", "payload": {"data": {"attributes":
//{"device": "sys/tg_test/1", "attribute": "double_scalar", "value": 184.15134018566408, "writeValue": 0.0, "timestamp": 1568283174.866954}}}}

//+

//{"type": "data", "payload": {"data": {"attributes":
//{"device": "sys/tg_test/1", "attribute": "short_scalar", "value": 188, "writeValue": 0, "timestamp": 1568283174.867977}}}}


static QString getIdentifier(QWebSocket *peer);

int countMaxFrames = 0;
int maxToSend = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    srand (time(NULL));

    ui->label->setScaledContents(true);

    timerSendSocket = new QTimer(this);
    connect(timerSendSocket, SIGNAL(timeout()), this, SLOT(timerCheck()));

    m_pWebSocketServer = new QWebSocketServer(QStringLiteral("Chat Server"), QWebSocketServer::NonSecureMode, this);
    if (m_pWebSocketServer->listen(QHostAddress::Any, port))
    {
        QTextStream(stdout) << "Chat Server listening on port " << port << '\n';
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
                this, &MainWindow::onNewConnection);
    }

    //QString exM = "{\"type\":\"start\",\"payload\":{\"query\":\"\nsubscription Attributes($fullNames: [String]!) {\n  attributes(fullNames: $fullNames) {\n    device\n    attribute\n    value\n    writeValue\n    timestamp\n  }\n}\",\"variables\":{\"fullNames\":[\"sys/tg_test/1/double_scalar\",\"sys/tg_test/1/short_scalar\"]}}}";
    //devicesList = devicesToList(exM);
}

MainWindow::~MainWindow()
{
    qDebug() << "Destroy";
    m_pWebSocketServer->close();
    delete ui;
}

void MainWindow::WriteTextBox(QString txt)
{
    ui->tb_recieve->appendPlainText(txt);
}

void MainWindow::on_bt_send_clicked()
{
    /* generate secret number between 1 and 999: */
    int randNum = rand() % 999 + 1;
    QString value = QString::number(randNum);
    QString time = QTime::currentTime().toString("hhmmss.zzz");
    qDebug() << "Send: " << value << " timestamp: " << time;
    QString txt = "{\"type\": \"data\", \"payload\": {\"data\": {\"attributes\": {\"device\": \"sys/tg_test/1\", \"attribute\": \"double_scalar\", \"value\": "+value+", \"writeValue\": 0.0, \"timestamp\": "+time+"}}}}";
    sendSocket(txt);
}

void MainWindow::on_bt_start_clicked()
{
    if(ui->in_time->value() != 0)
    {
        timerSendSocket->start(ui->in_time->value());
    }
}

void MainWindow::on_bt_clear_clicked()
{
    ui->tb_recieve->clear();
}

//WEB SOCKET

void MainWindow::onNewConnection()
{
    auto pSocket = m_pWebSocketServer->nextPendingConnection();
    qDebug() << getIdentifier(pSocket) << " connected!\n";
    ui->bt_start->setEnabled(true);
    pSocket->setParent(this);

    connect(pSocket, &QWebSocket::textMessageReceived,
            this, &MainWindow::processMessage);
    connect(pSocket, &QWebSocket::disconnected,
            this, &MainWindow::socketDisconnected);

    m_clients << pSocket;
}
//! [onNewConnection]

//! [processMessage]
void MainWindow::processMessage(const QString &message)
{
    qDebug() << message;
    ui->tb_recieve->appendPlainText("Recieved websocket: "+message);
    if(message.contains("fullNames")) {
        QStringList devicesList_tmp = {};
        devicesList_tmp = devicesToList(message);


        QSet<QString> mySet = devicesList_tmp.toSet();

        devicesList = QList<QString>::fromSet(mySet);
        numOfDevices = devicesList.length();

        for(int i=0; i<numOfDevices; i++)
        {
            qDebug() << devicesList_tmp[i];
            qDebug() << "Name: " << getName(devicesList_tmp[i]);
            qDebug() << "Type: " << getType(devicesList_tmp[i]);
        }

        if(ui->cb_autostart->isChecked())
        {
            if(ui->in_time->value() != 0)
            {
                timerSendSocket->start(ui->in_time->value());
            }
        }
    }
}
//! [processMessage]

//! [socketDisconnected]
void MainWindow::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    qDebug() << getIdentifier(pClient) << " disconnected!\n";
    ui->bt_start->setEnabled(false);
    timerSendSocket->stop();
    if (pClient)
    {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}

void MainWindow::sendSocket(QString data)
{
    for (QWebSocket *pClient : qAsConst(m_clients)) pClient->sendTextMessage(data);
}

void MainWindow::timerCheck()
{
    sendData();
}

static QString getIdentifier(QWebSocket *peer)
{
    return QStringLiteral("%1:%2").arg(peer->peerAddress().toString(),
                                       QString::number(peer->peerPort()));
}

void MainWindow::on_bt_stop_clicked()
{
    timerSendSocket->stop();
    countMaxFrames = 0;
}

QStringList MainWindow::devicesToList(QString str)
{
    str = str.remove(0,str.indexOf("\"fullNames\"")+13);
    str = str.remove(str.length()-4,4);
    str = str.replace("\"","");
    QStringList devices = str.split(",");
    numOfDevices = devices.length();
    return devices;
}

QString MainWindow::getName(QString str)
{
    QString returnStr = "";
    int count = 0;
    for(int i=0; i< str.length(); i++)
    {
        if(str.at(i)=='/' && count<3)
        {
            count++;
        }
        else if(count==3)
        {
            returnStr = str.remove(i-1,str.length());
        }
    }
    return returnStr;
}

QString MainWindow::getType(QString str)
{
    QString returnStr = "";
    int count = 0;
    for(int i=0; i< str.length(); i++)
    {
        if(str.at(i)=='/' && count<3)
        {
            count++;
        }
        else if(count==3)
        {
            returnStr = str.remove(0,i);
        }
    }
    return returnStr;
}

void MainWindow::on_bt_one_clicked()
{
    sendData();
}

void MainWindow::sendData()
{
    maxToSend = ui->in_many->value();
    if(countMaxFrames< maxToSend)
    {
        for(int i=0; i<numOfDevices; i++)
        {

            QString value;
            if(getType(devicesList[i]).contains("spectrum"))
            {
                /* generate secret number between 1 and 999: */
                value = "[";
                for(int i=0; i<countMaxFrames+1; i++)
                {
                    int randNum = rand() % 999 + 1;
                    value += QString::number(randNum)+",";
                }
                value += "]";
                value = value.replace(",]","]");
            }
            else {
                int randNum = rand() % 999 + 1;
                value = QString::number(randNum);
            }
            QString time = QTime::currentTime().toString("hhmmss.zzz");

            //QString Orig = "{\"type\": \"data\", \"payload\": {\"data\": {\"attributes\": {\"device\": \"sys/tg_test/1\", \"attribute\": \"double_scalar\", \"value\": "+value+", \"writeValue\": 0.0, \"timestamp\": "+time+"}}}}";
            QString txt = "{\"type\": \"data\", \"payload\": {\"data\": {\"attributes\": {\"device\": \""+getName(devicesList[i])+"\", \"attribute\": \""+getType(devicesList[i])+"\", \"value\": "+value+", \"writeValue\": 0.0, \"timestamp\": "+time+"}}}}";
            //qDebug() << "Orig: " << Orig;
            qDebug() << "txt: " << txt;
            sendSocket(txt);
        }

        countMaxFrames++;
        ui->pb_sending->setValue((int)countMaxFrames*100 / maxToSend);
    }
    else
    {
        timerSendSocket->stop();
        countMaxFrames = 0;
    }
}
